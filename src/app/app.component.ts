import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular Course';
  subtitle = 'With love by Technogi <3';
  heroType = 'is-link';
  heroSize = 'is-small';
}
