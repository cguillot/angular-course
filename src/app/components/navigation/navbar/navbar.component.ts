import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  links: object[] = [
    { link: "/", text: "Home" },
    { link: "/docs", text: "Documentation" },
    { link: "/help", text: "Help" },
    { link: "https://gitlab.com/delebrindel", text: "GitLab" },
    { link: "/help", text: "More", sublinks: [
      {link: "/about", text: "About"},
      {link: "/more-info", text: "More information"},
      {link: "https://tres0011.com", text: "Website"}
    ] }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
